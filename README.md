# [Neon White Speedruns][0]

## How to develop this

First, do a one-time setup:

1. Install Node
1. `cd` into the directory and install packages with `npm install`

`npm run build` will run a build.

You can see other scripts by running `npm run`. For example, `npm run format` will auto-format the code.

[0]: https://neonwhitespeedruns.com/
