import assert from "node:assert";
import * as durationFns from "duration-fns";

// TODO: Update this to allow hours
export function parse(str) {
  assert(typeof str === "string");

  const parts = str.split(":");
  assert(parts.length === 2);

  const [minutesStr, secondsStr] = parts;

  assert(/^\d+$/.test(minutesStr));
  const minutes = parseInt(minutesStr, 10);

  assert(/^\d{2}(?:.\d+)?$/.test(secondsStr));
  const seconds = parseFloat(secondsStr);
  assert(seconds < 60);

  return minutes * 60 + seconds;
}

const secondsToMilliseconds = (seconds) =>
  // We floor this to avoid floating point errors.
  Math.floor(seconds * 1000);

export const formatWithIso8601 = (seconds) =>
  durationFns.toString(secondsToMilliseconds(seconds));

// TODO: Update this to allow hours
export function formatWithColons(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds - minutes * 60;

  let secondsStr = remainingSeconds
    .toFixed(2)
    .replace(/0+$/, "")
    .replace(/\.$/, "");
  if (remainingSeconds < 10) {
    secondsStr = "0" + secondsStr;
  }

  return `${minutes}:${secondsStr}`;
}
