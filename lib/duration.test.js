import test from "ava";
import { parse, formatWithIso8601, formatWithColons } from "./duration.js";

const minute = 60;

test("parse", (t) => {
  const validTestCases = {
    "0:00": 0,
    "0:00.0": 0,
    "0:00.123": 0.123,
    "1:00": minute,
    "9:00": 9 * minute,
    "3:45": 3 * minute + 45,
    "3:45.67": 3 * minute + 45.67,
    "3:45.670000": 3 * minute + 45.67,
  };
  for (const [input, expected] of Object.entries(validTestCases)) {
    const actual = parse(input);
    t.is(actual, expected, `Parsed ${JSON.stringify(input)} incorrectly`);
  }

  const invalidInputs = [
    undefined,
    null,
    12,
    "",
    "0",
    "0:0",
    "0:1",
    "1:23:45",
    "1:60",
    "1:61",
    "1:99",
    "1:",
    "1::0.23",
    "1::2",
    " 1:00",
    "2:00 ",
  ];
  for (const input of invalidInputs) {
    t.throws(
      () => parse(input),
      undefined,
      `Expected ${JSON.stringify(input)} to throw`
    );
  }
});

test("formatWithIso8601", (t) => {
  // These tests are simple because it relies on duration-fns.
  const testCases = [
    [1, "PT1S"],
    [12.345, "PT12,345S"],
  ];
  for (const [input, expected] of testCases) {
    const actual = formatWithIso8601(input);
    t.is(actual, expected, `Formatted ${JSON.stringify(input)} incorrectly`);
  }
});

test("formatWithColons", (t) => {
  const testCases = [
    [0, "0:00"],
    [0.123, "0:00.12"],
    [0.1234, "0:00.12"],
    [12, "0:12"],
    [12.34, "0:12.34"],
    [59, "0:59"],
    [60, "1:00"],
    [65, "1:05"],
    [65.43, "1:05.43"],
    [120, "2:00"],
    [121, "2:01"],
  ];
  for (const [input, expected] of testCases) {
    const actual = formatWithColons(input);
    t.is(actual, expected, `Formatted ${JSON.stringify(input)} incorrectly`);
  }
});
