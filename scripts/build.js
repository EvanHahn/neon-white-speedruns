#!/usr/bin/env node

import * as path from "path";
import * as fs from "node:fs/promises";
import got from "got";
import * as pug from "pug";
import { minBy } from "lodash-es";
import slugify from "@sindresorhus/slugify";
import * as duration from "../lib/duration.js";

const SUBMISSIONS_SHEET_URL =
  "https://opensheet.elk.sh/1MBv2FFYBAK1e9n-v0ADm4GJ60fib4nineU3TtQzapQ8/Submissions";
const SUBMISSION_FORM_URL = "https://example.com"; // TODO

const __dirname = path.dirname(new URL(import.meta.url).pathname);
const projectRoot = path.join(__dirname, "..");
const distPath = path.join(projectRoot, "dist");
const templatePath = path.join(projectRoot, "templates");
const indexHtmlPath = path.join(distPath, "index.html");
const indexTemplatePath = path.join(templatePath, "index.pug");
const levelTemplatePath = path.join(templatePath, "level.pug");
const playerTemplatePath = path.join(templatePath, "player.pug");

function formatChapterName(chapter) {
  return Number.isNaN(parseInt(chapter.id))
    ? chapter.englishName
    : `Chapter ${chapter.id}: ${chapter.englishName}`;
}

function getLevelUrl(chapterId, levelNumber) {
  return `/level/${chapterId}-${levelNumber}`;
}

// TODO: rename this to getPlayerUrl
function getRunnerUrl(player) {
  return `/player/${slugify(player)}`;
}

async function loadSubmissions(levelData) {
  console.time("loadSubmissions");

  const validLevelIds = new Set();
  for (const chapter of levelData.chapters) {
    for (const levelIndex of chapter.levels.keys()) {
      const levelNumber = levelIndex + 1;
      validLevelIds.add(chapter.id + "-" + levelNumber);
    }
  }

  const warnAboutInvalidRow = (row, property) => {
    console.warn(
      `Row ${JSON.stringify(row)} has an invalid ${property}. Skipping it.`
    );
  };

  const trim = (maybeString) =>
    typeof maybeString === "string" ? maybeString.trim() : "";

  const result = [];

  for (const row of await got(SUBMISSIONS_SHEET_URL).json()) {
    const isApproved = trim(row["Approved?"]).toLowerCase() === "approved";
    if (!isApproved) {
      continue;
    }

    const level = trim(row["Level"]);
    const isValidLevel = validLevelIds.has(level);
    if (!isValidLevel) {
      warnAboutInvalidRow(row, "level");
      continue;
    }
    const [chapterId, rawLevelNumber] = level.split("-");
    const levelNumber = Number(rawLevelNumber);

    const player = trim(row["Player"]);
    // TODO: Additional validations, for strange characters
    const isValidPlayer = Boolean(player.length && player.length < 200);
    if (!isValidPlayer) {
      warnAboutInvalidRow(row, "player");
      continue;
    }

    const console = trim(row["Console"]).toLowerCase();
    const isValidConsole = console === "pc" || console === "switch";
    if (!isValidConsole) {
      warnAboutInvalidRow(row, "console");
      continue;
    }

    const time = trim(row["Time"]);
    let timeInSeconds;
    try {
      timeInSeconds = duration.parse(time);
    } catch (err) {
      warnAboutInvalidRow(row, "time");
      continue;
    }

    // TODO: Parse submission date
    const submissionDate = new Date();

    const videoLink = trim(row["Video link"]);
    try {
      new URL(videoLink);
    } catch (err) {
      warnAboutInvalidRow(row, "videoLink");
      continue;
    }

    const runnerNotes = trim(row["Runner notes"]);

    result.push({
      chapterId,
      levelNumber,
      player,
      console,
      timeInSeconds,
      submissionDate,
      videoLink,
      runnerNotes,
    });
  }

  console.timeEnd("loadSubmissions");

  return result;
}

async function loadLevelData() {
  const levelDataPath = path.join(projectRoot, "level_data.json");
  const levelDataRaw = await fs.readFile(levelDataPath, "utf8");
  return JSON.parse(levelDataRaw);
}

async function fetchAndValidateData() {
  const levelData = await loadLevelData();
  return {
    submissions: await loadSubmissions(levelData),
    levelData,
  };
}

async function generateIndexHtml({ submissions, levelData }) {
  const chapters = levelData.chapters.map((chapter) => {
    return {
      id: chapter.id,
      formattedName: formatChapterName(chapter),
      consoles: ["pc", "switch"].map((consoleId) => ({
        id: consoleId,
        levels: chapter.levels.map((levelEnglishName, levelIndex) => {
          const levelNumber = levelIndex + 1;

          const allSubmissions = submissions.filter(
            (submission) =>
              submission.console === consoleId &&
              submission.chapterId === chapter.id &&
              submission.levelNumber === levelNumber
          );
          const fastestSubmission = minBy(allSubmissions);

          return {
            number: levelIndex + 1,
            englishName: levelEnglishName,
            ...(fastestSubmission
              ? {
                  player: fastestSubmission.player,
                  time: {
                    durationString: duration.formatWithIso8601(
                      fastestSubmission.timeInSeconds
                    ),
                    humanString: duration.formatWithColons(
                      fastestSubmission.timeInSeconds
                    ),
                  },
                  videoLink: fastestSubmission.videoLink,
                }
              : {}),
          };
        }),
      })),
      type: chapter.type ?? "normal",
    };
  });

  await fs.writeFile(
    indexHtmlPath,
    pug.renderFile(indexTemplatePath, {
      SUBMISSION_FORM_URL,
      getLevelUrl,
      getRunnerUrl,
      chapters,
    })
  );
}

async function generateLevelHtml({
  submissions,
  chapter,
  levelName,
  levelNumber,
}) {
  const levelSlug = `${chapter.id}-${levelNumber}`;

  const levelHtmlFolderPath = path.join(distPath, "level", levelSlug);
  const levelHtmlPath = path.join(levelHtmlFolderPath, "index.html");

  const consoles = ["pc", "switch"].map((consoleId) => ({
    id: consoleId,
    submissions: submissions
      .filter(
        (submission) =>
          submission.console === consoleId &&
          submission.chapterId === chapter.id &&
          submission.levelNumber === levelNumber
      )
      .map((submission) => ({
        ...submission,
        time: {
          durationString: duration.formatWithIso8601(submission.timeInSeconds),
          humanString: duration.formatWithColons(submission.timeInSeconds),
        },
      }))
      .sort((a, b) => a.timeInSeconds - b.timeInSeconds),
  }));

  await fs.mkdir(levelHtmlFolderPath, { recursive: true });
  await fs.writeFile(
    levelHtmlPath,
    pug.renderFile(levelTemplatePath, {
      SUBMISSION_FORM_URL,
      getLevelUrl,
      getRunnerUrl,
      chapter: {
        ...chapter,
        formattedName: formatChapterName(chapter),
      },
      level: {
        slug: levelSlug,
        englishName: levelName,
      },
      consoles,
    })
  );
}

async function generateLevelsHtml({ submissions, levelData }) {
  await Promise.all(
    levelData.chapters.map((chapter) =>
      Promise.all(
        chapter.levels.map((level, levelIndex) =>
          generateLevelHtml({
            submissions,
            chapter,
            levelName: level,
            levelNumber: levelIndex + 1,
          })
        )
      )
    )
  );
}

async function generatePlayerHtml({ submissions, player }) {
  const playerSlug = slugify(player);
  const playerHtmlFolderPath = path.join(distPath, "player", playerSlug);
  const playerHtmlPath = path.join(playerHtmlFolderPath, "index.html");

  await fs.mkdir(playerHtmlFolderPath, { recursive: true });
  await fs.writeFile(
    playerHtmlPath,
    pug.renderFile(playerTemplatePath, {
      getLevelUrl,
      player,
      submissions: submissions
        .filter((s) => s.player === player)
        .map((submission) => ({
          ...submission,
          time: {
            durationString: duration.formatWithIso8601(
              submission.timeInSeconds
            ),
            humanString: duration.formatWithColons(submission.timeInSeconds),
          },
        })),
    })
  );
}

async function generatePlayersHtml({ submissions }) {
  const players = new Set(submissions.map((s) => s.player));
  await Promise.all(
    [...players].map((player) => generatePlayerHtml({ submissions, player }))
  );
}

async function generateHtml() {
  const data = await fetchAndValidateData();
  await Promise.all([
    generateIndexHtml(data),
    generateLevelsHtml(data),
    generatePlayersHtml(data),
  ]);
}

async function copyStaticFiles() {
  const staticPath = path.join(projectRoot, "static");
  const staticFiles = await fs.readdir(staticPath);
  await Promise.all(
    staticFiles.map((file) =>
      fs.cp(path.join(staticPath, file), path.join(distPath, file), {
        recursive: true,
      })
    )
  );
}

async function main() {
  console.time("build");
  await fs.rm(distPath, { force: true, recursive: true });
  await fs.mkdir(distPath, { recursive: true });
  await Promise.all([generateHtml(), copyStaticFiles()]);
  console.timeEnd("build");
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
